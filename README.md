# Aplicación utilizando npm
Universidad Autónoma de Chihuahua.
Ingeniería en ciencias de la computación. 
WEB PLATAFORMS 7CC2. 
Gabriel Jesús Bustillos Fierro (353267).

Los siguientes scripts son el resultado de nuestra primera aplicación con node.js utilizando dependencias.

### Prerequisitos

Deberás contar con npm para verificar si cuentas con el simplemente ve a tu terminal y ejecuta:
```
npm -v 
```
Así como también con node js lo puedes verificar con: 
```
node -v 
```

### Instalación en local

Clona el proyecto en la carpeta de tu preferencia de la siguiente manera:
```json
git clone: https://gitlab.com/a353267/app-con-npm.git
```

Después de haber clonado el repositorio te arrojará una serie de archivos.
Deberás de abrir una terminal y estar en la ruta donde tienes todo lo de el repositorio clonado.
Una vez ahí deberas ejecutar el comando:
```
npm run
```
Esto ejecutará el archivo index.js 
Si lo que deseas es ejecutar el archivo de pruebas tendrás que escribir el siguiente comando:
```
npm run test
```
Si todo sale bien deberá de imprimirse en pantalla lo plasmado en los archivos.

Si deseas editarlos o realizar alguna modificación lo puedes hacer desde tu editor de texto preferido,en mi caso utilizo Visual Studio Coode el cual te recomiendo (https://code.visualstudio.com/).


# ¿Dudas?, ¿Sugerencias?
Si quieres que realice alguna mejora general, abre un "issue" aquí en gitlab. Lo arreglaré lo antes posible.
