
const sum = require('../index');
const assert = require('assert');

describe("Probar la suma de dos números", () => {
   //Afirmar que 5+5 = 10 
    it("Cinco más cinco es diez", ()=> {
        assert.equal(10, sum(5,5));
    });

    //AFIRMAMOS QUE 5+5 != 55
    it("Cinco más cinco no son 55", ()=> {
        assert.notEqual(55, sum(5,5));
    });

});